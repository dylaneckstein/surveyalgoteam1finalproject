package algoteam1finalproject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/** Created by team 1 (Dylan Eckstein, Matt Aylward, Hunter Root, and Peyton Mizera)
 *  For Survey of Algorithms class April 20, 2018.
 * REPO HERE => https://bitbucket.org/dylaneckstein/surveyalgoteam1finalproject
 */
public class AlgoTeam1FinalProject {

    static ArrayList<Double> regTimes = new ArrayList();     
    static ArrayList<Double> boyerTimes = new ArrayList();   
    public static Map<Character, Integer> shift = new HashMap<>();  //this is our mismatch "table"
    
    //t is the longer text, p is the shorter text we are trying to match in t
    //n is the length of t and m is the length of p
    //t < p and n < m
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double startTime, endTime, 
                sumReg = 0, sumBoyer = 0,
                avgReg = 0, avgBoyer = 0;
        int numRuns = 100;  //number of iterations is a constant 100 for our project

        //GET INPUT
        System.out.println("Size of P: ");
        int pSize = input.nextInt();
        System.out.println("Size of T: ");
        int tSize = input.nextInt();

        //start loop
        for (int i = 0; i < numRuns; i++) {
            //create p and t
            char[] p = randCharArr(pSize);
            char[] t = createT(tSize, p);

            // Start of timing for the regular string search method
            startTime = System.nanoTime();
            regSearch(t, p);
            endTime = System.nanoTime();
            regTimes.add(endTime - startTime);
            
            // Start of timing for the Boyer Moore Horspool Algorithm
            startTime = System.nanoTime();
            boyerSearch(t, p);
            endTime = System.nanoTime();
            boyerTimes.add(endTime - startTime);
            //=======================END FOR LOOP===============================
        }

        // Our times are stored in the regTime array so we need to sum the values and get the average
        for (int i = 0; i < numRuns; i++) {
            sumReg += regTimes.get(i);
        }
        avgReg = (sumReg/numRuns) / 1000000; //divide by 10^6 to get milliseconds
        // Our times are stored in the boyerTimes array so we need to sum the values and get the average 
        for (int i = 0; i < numRuns; i++) {
            sumBoyer += boyerTimes.get(i);
        }
        avgBoyer = (sumBoyer/numRuns) / 1000000; //divide by 10^6 to get milliseconds

        //output
        System.out.println("100 iterations");
        System.out.println("The top number is the average time of Simple Text Search"
                + " and the bottom number is the average time of Boyer-Moore-Horspool search");
        System.out.println("Both are in MILLISECONDS");
        System.out.printf("%f%n", avgReg);
        System.out.printf("%f%n", avgBoyer);
        //=======================END OF MAIN===============================
    }
    
    public static boolean regSearch(char[] charArray, char[]word){
        int fullCharArray = charArray.length; // n
        int phrase = word.length; // m
        // Initialize the outer loop counter to zero
        int i = 0;
        // This loop is steps us through the entire character array of values
        while(i + phrase <= fullCharArray){
            // Initialize the inner loop counter to zero
            int j = 0;
            // This loop steps us though the entire current selection
            while(charArray[i + j] == word[j]){
                j = j + 1;
                if(j >= phrase) {
                    //System.out.println("r:" + i);
                    return true;
                }
            }
            i = i + 1;
        } 
        return false;
    }
    
    //takes in p and t and returns true if p is a substring within t
    public static boolean boyerSearch(char[] t, char[]p){
        int n = t.length;
        int m = p.length;
        createMap(t, p);    //creates our mismatch table
        int j;              //our match counter
        int i = 0;          //the right most character of p will first check with t[i]
        while(i + m <= n){
            j = m - 1;
            while(t[i + j] == p[j] ){   //this while loop is for every time we check p with t at t[i]
                j = j - 1;
                if(j < 0){      //once j<0 we know that the substring found a match in the string at index i
                    return true;
                }
            }
            i = i + shift.get(t[ i + m - 1]);   //we call our mismatch table to find how much we shift
        }
        return false;   //should never reach here
    }
    
    //this returns a randomly filled char array of specified size
    //we only use this to generate p
    public static char[] randCharArr(int size){
        char[] charArr = new char[size];
        Random rand = new Random();
        char[] charList = {'a', 't', 'g', 'c'};     //this is pretty much a set that we choose from
        int r = 0;     //this will specify which char is chosen
        for (int i = 0; i < size; i++) {
            r = rand.nextInt(4);
            charArr[i] = charList[r];
        }
        return charArr;
    }
    
    //this creates and returns our large char array t and is divided into 3 parts
    public static char[] createT(int size, char[] p){
        char[] charArr = new char[size];
        Random rand = new Random();
        int part1 = rand.nextInt(size - p.length);  //this is the location that p will be inserted
        char[] charList = {'a', 't', 'c', 'g'};
        int r = 0;
        //randfill up to where we are putting p
        for (int i = 0; i < part1; i++) {
            r = rand.nextInt(4);
            charArr[i] = charList[r];
        }
        int pCounter = 0;
        //insert in p
        for (int i = part1; i < p.length + part1; i++) {
            charArr[i] = p[pCounter];
            pCounter++;
        }
        //fill the rest of the array
        for (int i = part1 + p.length; i < size; i++) {
            r = rand.nextInt(4);
            charArr[i] = charList[r];
        }
        return charArr;
    }
    
    //creates the mismatch table
    public static void createMap(char[] t, char[] p){
        int m = p.length;
        shift = new HashMap();
        //we start at the end of p and work our way to the beginning
        //we only add a value for a character if it has none so we don't overwrite values
        //we stop once we have values for all 4 character in our alphabet
        for (int k = m-2; k > 0; k--) {
            shift.putIfAbsent(p[k], m - 1 - k);
            if(shift.size() == 4){
                break;
            }
        }
        //this is just in case p is very small and doesn't contain all 4 characters
        if (shift.size() < 4){
            shift.putIfAbsent('a', m);
            shift.putIfAbsent('t', m);
            shift.putIfAbsent('c', m);
            shift.putIfAbsent('g', m);
        }
    }
    
    
    
}
